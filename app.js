'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('log4js').getLogger('cbe');
const path = require('path');
const sshService = require('./ssh.service.js');
const compileRoutes = require('./routes/compile.routes.js');
const app = express();

const server = app.listen(3000, () => {
    logger.trace('Servidor online');
});

app.use(bodyParser.json());

app.use('/vendor', express.static('bower_components'));
app.use('/resources', express.static('resources'));
app.use('/views', express.static('views'));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/views/main.html'));
});

app.use(compileRoutes);

app.get('/executeCompile', (req, res) => {
    sshService();
});