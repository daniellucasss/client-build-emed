const fs = require('fs');
const path = require('path');
const node_ssh = require('node-ssh');
const ssh = new node_ssh();

module.exports = () => {
    ssh.connect({
    host: 'localhost',
    username: 'steel',
    privateKey: '/home/steel/.ssh/id_rsa'
    }).then(() => {
        ssh.exec('./compileAll.sh', [], {}).then(function(result) {
            console.log('STDOUT: ' + result)
        });
    });
}