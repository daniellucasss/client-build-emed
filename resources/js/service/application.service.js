angular.module('application').factory('applicationService', applicationService);

function applicationService($http, $rootScope) {
    const sendCommandCompile = function(compilers) {
        return $http.post('/compile', JSON.stringify(compilers));
    }

    const getCompiledFileAsArray = function() {
        return $http.get('/compile');
    }

    const executeCompile = function() {
        return $http.get('/executeCompile');
    }

    return {
        sendCommandCompile: sendCommandCompile,
        getCompiledFileAsArray: getCompiledFileAsArray,
        executeCompile: executeCompile
    }
};