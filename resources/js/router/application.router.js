application.config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise('/main');
	
    $stateProvider
        .state('main', {
            url: '/main',
            templateUrl: '/views/index.html',
            controller: 'mainController'
        })
});