application.controller('mainController', mainController);

function mainController($scope, applicationService) {
    $scope.compilers = [{}];

    $scope.sendCompile = function() {
        swal({
            title: 'Gerar CompileAll',
            text: 'Deseja gerar um novo arquivo?<br>(Ao gerar novo arquivo o antigo será sobrescrito)',
            type: 'info',
            html: true,
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function(){
            applicationService.sendCommandCompile($scope.compilers)
            .then(function successCallback(res) {
                swal('Arquivo gerado com sucesso!', '', 'success');
            }, function errorCallback(res) {
                swal('Erro ao gerar arquivo!', '', 'error');
            })
        });

    }

    $scope.addCompiler = function() {
        $scope.compilers.push({});
    }

    $scope.getCompiledFileAsArray = function() {
        applicationService.getCompiledFileAsArray()
            .then(function successCallback(res) {
                for(let cont = 0; cont < res.data.data.length; cont++) {
                    $scope.compilers = res.data.data;
                }
            }, function errorCallback(res) {
            });
    }

    $scope.executeCompile = function() {
        applicationService.executeCompile()
            .then(function successCallback(res) {
            }, function errorCallback(res) {
            });
    }

    $scope.init = function() {
        $scope.getCompiledFileAsArray();
    }

    $scope.init();
}