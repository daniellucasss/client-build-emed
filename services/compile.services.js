'use strict'
const fs = require('fs');
const os = require('os');
const streamString = require('stream-string');
const logger = require('log4js').getLogger('cbe');

const createCompileFile = (compilers) => {
    const stream = fs.createWriteStream('compileAll.sh');
    stream.once('open', () => {
        for(let cont = 0; cont < compilers.length; cont++) {
            stream.write('./compile.sh ' + compilers[cont].job + ' ' 
                + compilers[cont].banco + ' ' + compilers[cont].versao + ' ' + compilers[cont].warFinal + ' ' + os.EOL);
        }
        stream.end();
    });
}

const returnCompiledFileAsArray = (res) => {
    const stream = fs.createReadStream('compileAll.sh');
    if(stream) {
        let returnArray = [];
        streamString(stream).then(data => {
            const readString = data;
            let arr = readString.split('./compile.sh');
            for(let cont = 1; cont < arr.length; cont++) {
                returnArray.push(returnCompilerByString(arr[cont].trim().split(' ', 4)));
            }
            return returnArray;
        }).then(returnArray => {
            res.status(200).json({data: returnArray});
        }).catch(err => {
        });
    } else {
        res.status(500).json({data: null});
    }
}

const returnCompilerByString = (str) => {
    return {
        job: str[0],
        banco: str[1],
        versao: str[2],
        warFinal: str[3]
    }
}


module.exports = {
    createCompileFile: createCompileFile,
    returnCompiledFileAsArray: returnCompiledFileAsArray
}