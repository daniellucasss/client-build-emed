'use strict'
const express = require('express');
const router = express.Router();
const streamString = require('stream-string');
const logger = require('log4js').getLogger('cbe');
const compileServices = require('./../services/compile.services.js');

router.get('/compile', (req, res) => {
    compileServices.returnCompiledFileAsArray(res);
});

router.post('/compile', (req, res) => {
    try {
        compileServices.createCompileFile(req.body);
        res.status(200).json({message: 'mensagem sucesso'});
    } catch(err) {
        res.status(400).json({message: 'mensagem erro'});
    }
});

module.exports = router;